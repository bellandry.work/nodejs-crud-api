const express = require('express')
const connectDB = require('./config/db')
const dotenv = require('dotenv').config()
port = process.env.PORT || 3000

/* Connexion to database */
connectDB()

const app = express()

/* Middle to manage request data */
app.use(express.json())
app.use(express.urlencoded({ extended: true }))


app.use('/post', require('./routes/post.routes'))

app.use('/comment', require('./routes/comment.routes'))

app.listen(port, () => {
  return console.log(`Server started on port ${port} \nVisit your app at http://localhost:${port}`)
})