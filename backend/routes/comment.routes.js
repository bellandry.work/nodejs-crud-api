const express = require('express')
const router = express.Router()

router.get('/:id', (req, res) => {
  res.json({ msg: `Get comments of post ${req.params.id}` })
})

router.post('/post/:id', (req, res) => {
  console.log(req.body);
  res.json({ msg: `add Comment ${req.body.comment} to post ${req.params.id}` })
})

router.put('/:id', (req, res) => {
  res.json({ msg: `Modifier le commentaire ${req.params.id}` })
})

router.delete('/:id', (req, res) => {
  res.json({ msg: `Supprimer le commentaire ${req.params.id}` })
})

router.patch('/like-comment/:id', (req, res) => {
  res.json({ msg: `Le commentaire ${req.params.id} a ete liké` })
})

router.patch('/dislike-comment/:id', (req, res) => {
  res.json({ msg: `Le commentaire ${req.params.id} a ete disliké` })
})

module.exports = router