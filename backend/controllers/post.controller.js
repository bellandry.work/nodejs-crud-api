const PostModel = require('../models/post.model');

module.exports.getPosts = async (req, res) => {
  const posts = await PostModel.find()
  res.status(200).json(posts)
}

module.exports.setPosts = async (req, res) => {
  if (!req.body.title || !req.body.content || !req.body.author) {
    res.status(400).json({ message: 'Missing some datas' })
    return 0
  }

  const post = await PostModel.create({
    title: req.body.title,
    content: req.body.content,
    author: req.body.author,
    likes: [],
    comments: [],
  })

  res.status(200).json(post)
}

module.exports.editPost = async (req, res) => {
  try {
    await PostModel.findByIdAndUpdate(req.params.id,
      req.body,
      {
        new: true
      }).then((data) => res.status(200).send(data))
  } catch (error) {
    res.status(400).json(error.message)
  }
}

module.exports.deletePost = async (req, res) => {
  try {
    await PostModel.findByIdAndDelete(
      req.params.id,
      {
        new: true
      }
    )

    res.status(200).json({ message: 'Post deleted' })
  } catch (error) {
    res.status(400).json(error.message)
  }
}

module.exports.likePost = async (req, res) => {
  try {
    const { userId } = req.body
    const post = await PostModel.findById(req.params.id)

    if (userId && !post.likers.includes(userId)) {
      await PostModel.findByIdAndUpdate(post,
        {
          $push: { likers: userId }
        },
        {
          new: true
        }).then((data) => res.status(200).send(data))
    }
    res.status(200).json(post)
  } catch (error) {
    res.status(404).json(error.message)
  }
}

module.exports.dislikePost = async (req, res) => {
  try {
    const updatedPost = await PostModel.findByIdAndUpdate(req.params.id,
      {
        $pull: { likers: req.body.userId }
      },
      {
        new: true
      })

    res.status(200).json(updatedPost)
  } catch (error) {
    res.status(404).json(error.message)
  }
}
