NodeJS simple API app

This is just a simple API App to teach how to use MERN Stack to make CRUD operations

To test it out there you can use the following command

npm install

scp .example.env .env

Create your mongoDB database and pass it to the MONGODB_URI in the env file.

Then run the app with the following command

npm run start